FROM maven:3.6.3-openjdk-8
#
ENV TZ=America/El_Salvador
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
#
MAINTAINER vhuezo <vhuezo@hightech-corp.com>

#
RUN apt update &&  apt -y install wget curl dirmngr apt-transport-https lsb-release ca-certificates
RUN curl -sL https://deb.nodesource.com/setup_12.x |  bash -
RUN apt -y install nodejs
RUN apt -y  install gcc g++ make
#RUN apt update && apt install nodejs npm -y

#
RUN npm install -g newman
RUN npm install -g newman-reporter-htmlextra
RUN npm install newman --save-dev
RUN npm install newman-reporter-csv --save-dev

